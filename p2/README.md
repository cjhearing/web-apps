# LIS 4381

## Christian Hearing

### Project 2:


> #### P2 PHP Form Validation
  #### Adds edit and delete functionality to the database
#### Assignment Screenshots:
*Screenshot of home*:

![img1](img/home.PNG)

*Screenshot of processing data*:

![img2](img/test1.PNG)

*Second screenshot of edit functionality*:

![img3](img/test2.PNG)

*Second screenshot of homepage data*:

![img4](img/homepage.PNG)

*Second screenshot of RSS feed data*:

![img5](img/rssfeed.PNG)
#### Assignment Links:

[http://localhost/repos/lis4381/](http://localhost/repos/lis4381/index.php "Localhost link")
