# LIS 4381

## Christian Hearing

### Assignment # Requirements:

1. [A1 README.md](a1/README.md "My A1 README.md file")

	* Installed AMPPS and JDK.
	* Installed Android Studio and created first app.
    * Created Bitbucket account and repositories.
    * Pushed readme files to bitbucket repository.
    * Provide screenshots of installations.
    * Provide git command descriptions.

2. [A2 README.md](a2/README.md "My A2 README.md file")

	* Installed API 24 for Android Studio.
	* Created required strings inside strings.xml file.
    * Created TextView items and ImgView items for first user interface.
    * Created a button that links to the second user interface.
    * Created TextView items for the second user interface.

3. [A3 README.md](a3/README.md "My A3 README.md file")

    * Created user interface1 using textview, spinner, and button widgets.
    * Created user interface2 using java number format and control structures.
    * Built a petstore relation database in MySQL Workbench.
    * Ran SQL queries to test database.

4. [P1 README.md](p1/README.md "My P1 README.md file")

	* Created user interface1 using textview, added image and button borders.
    * Created user interface2 with contact information.
    * Changed background colors for both interfaces, added image to interface1

5. [A4 README.md](a4/README.md "My A4 README.md file")

    * Downloaded bootstrap and required files from github.
    * Created required file structures.
    * Made user forms for data entry.
    * Validated user input on the client side.

5. [A5 README.md](a5/README.md "My A5 README.md file")

    * Created a database connection for A5.
    * Disabled client side data validation for testing.
    * Enabled server side data validation.
    * Tested with valid and invalid data.

6. [P2 README.md](p2/README.md "My P2 README.md file")

    * Created a database connection for P2.
    * Added delete functionality to database.
    * Added edit functionality to database.
    * Tested with valid and invalid data.
