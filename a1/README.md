# LIS 4381

## Christian Hearing

### Assignment # Requirements:


> #### Git commands w/short descriptions:

1. git init - creates an empty git repository	
2. git status - shows the working tree status, lists the files you've changed
3. git add - sends a file from the working directory to the index
4. git commit - sends the changed file from the index to the head
5. git push - sends all modified local files to the remote repository
6. git pull - merges the file from your remote repository into your local repository
7. git checkout - switch branches or restore working tree files

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.PNG)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.PNG)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.PNG)


#### Repository Links:


[bitbucketstationlocations Link](https://bitbucket.org/cjhearing/bitbucketstationlocations/ "Bitbucket Station Locations")


[myteamquotes Link](https://bitbucket.org/cjhearing/myteamquotes "My Team Quotes Tutorial")
