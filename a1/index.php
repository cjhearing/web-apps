<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="LIS4381 Online Portfolio">
	<meta name="author" content="Christian Hearing">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Assignment 1</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" />

<!-- Bootstrap for responsive, mobile-first design. -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous" />

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous" />

<!-- Form validation styling. -->
<link rel="stylesheet" href="css/formValidation.min.css" />

<!-- Custom styles with this template -->
<link href="../css/starter-template.css" rel="stylesheet" />

<!-- jQuery DataTables: http://www.datatables.net/ //-->
<!-- jQuery DataTables CDN: https://cdn.datatables.net/ //-->

<link rel="stylesheet" type=""text/css" https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<link rel="stylesheet" type=""text/css" href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css" />

<link rel="stylesheet" href="../css/img_padding.css" /> <!--padding images -->

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->


</head>
<body>

	<?php include_once("../global/nav_global.php"); ?>

	<div class="container">
		 <div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>
				</div>
			</div> <!-- end starter-template -->

	<div class="row">
		<div class="col-md-12">
			<h2>Assignment 1 Overview</h2>
			<p>For assignment 1, the task was to set up an apache web server using AMPPS, explore git commands, and create a basic Andorid application.</p>
			<h3>Git Commands</h3>
			<ol>
				<li>git init - creates an empty git repository</li>
				<li>git status - shows the working tree status, lists the files you've changed</li>
				<li>git add - sends a file from the working directory to the index</li>
				<li>git commit - sends the changed file from the index to the head</li>
				<li>git push - sends all modified local files to the remote repository</li>
				<li>git pull - merges the file from your remote repository into your local repository</li>
				<li>git checkout - switch branches or restore working tree files</li>
			</ol>
			<h3>Assignment Screenshots</h3>
			<img class="img-responsive img-padding" src="img/ampps.PNG" alt="ampps">
			<img class="img-responsive img-padding" src="img/jdk_install.png" alt="jdk_install">
			<img class="img-responsive img-padding" src="img/android.PNG" alt="android">

		</div> <!-- end col -->
	</div> <!-- end row -->

	<div class="starter-template">
		<?php include_once "global/footer.php";?>
	</div>


</div> <!-- end container -->


<!-- Bootstrap core JavaScript: jQuery necessary for Bootstrap's JavaScript plugins
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
		<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.3.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<!-- jQuery DataTables: http://www.datatables.net/ //-->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="js/ie10-viewport-bug-workaround.js"></script>

	<script>
	 $(document).ready(function(){
    $('#myTable').DataTable();
});
	</script>


</body>
</html>
