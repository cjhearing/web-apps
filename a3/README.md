# LIS 4381

## Christian Hearing

### Assignment # Requirements:


> #### My Event App

#### Assignment Screenshots:

*Screenshot of petstore ERD*:

![Diagram](img/erd.png)

*Screenshot of first user interface*:

![Interface 1](img/app1.png)

*Screenshot of second user interface*:

![Interface 2](img/app2.png)

#### Assignment Links:

[A3 a3.mwb](a3.mwb "My A3 .mwb File")

[A3 a3.sql](a3.sql "My A3 .sql File")

