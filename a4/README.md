# LIS 4381

## Christian Hearing

### Assignment 4:


> #### A4 PHP Form Validation

#### Assignment Screenshots:
*Screenshot of home*:

![img1](img/a4_home.png)

*Screenshot of validated data*:

![img1](img/a4_validated.PNG)

*Second screenshot of validated data*:

![img2](img/a4_unvalidated.PNG)

#### Assignment Links:

[http://localhost/repos/lis4381/](http://localhost/repos/lis4381/index.php "Localhost link")
